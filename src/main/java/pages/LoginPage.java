package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utils.SeleniumDriver;

public class LoginPage {
	
	String url;
	
	@FindBy(css="#user")
	public WebElement username_input;
	
	@FindBy(css="#pass")
	public WebElement password_input;
	
	@FindBy(css="#c187 > div > form > fieldset > div:nth-child(4) > input[type=submit]")
	public WebElement login_button;
	
	
	public  LoginPage()
	{
		PageFactory.initElements(SeleniumDriver.getDriver(), this);
		this.url = SeleniumDriver.getBaseUrl() + "home/login/";
	}
	
	public String getUrl()
	{
		return this.url;
		
	}
	
	public void usernameInput(String username)
	{
		this.username_input.sendKeys(username);	
	}
	
	public void passwordInput(String password)
	{
		this.password_input.sendKeys(password);
	}
	
	public void login()
	{
		this.login_button.click();
		
	}
}
