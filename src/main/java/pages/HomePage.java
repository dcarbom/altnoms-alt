package pages;

import org.openqa.selenium.support.PageFactory;

import utils.SeleniumDriver;

public class HomePage {
	
	String url;
	
	public  HomePage()
	{
		PageFactory.initElements(SeleniumDriver.getDriver(), this);
		this.url = SeleniumDriver.getBaseUrl() + "home/";
	}
	
	public String getUrl()
	{
		return this.url;
		
	}
	
	public void goToPage(String url)
	{
		SeleniumDriver.openPage(url);
	}
	
}
