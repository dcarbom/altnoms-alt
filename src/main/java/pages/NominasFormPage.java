package pages;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.SeleniumDriver;

public class NominasFormPage {
	
	String url;
	String url_redirection;
	
	@FindBy(css="#usuario")
	private WebElement usuario_input;
	
	@FindBy(css="#password")
	private WebElement password_input;
	
	@FindBy(css="#nif")
	private WebElement nif_input;
	
	@FindBy(css="#fn_dia")
	private WebElement fn_dia_select;
	
	@FindBy(css="#fn_mes")
	private WebElement fn_mes_select;
	
	@FindBy(css="#fn_ano")
	private WebElement fn_ano_select;
	
	@FindBy(css="#mes")
	private WebElement mes_select;
	
	@FindBy(css="#ano")
	private WebElement ano_select;
	
	@FindBy(css="#c246 > div > div > div > form > table:nth-child(12) > tbody > tr > td:nth-child(4) > div > input:nth-child(1)")
	private WebElement buscar_button;
	
	private static final String USER = System.getProperty("usr");
	private static final String PWD = System.getProperty("pwd");
	private static final String NIF = System.getProperty("nif");
	private static final String FN_DIA = System.getProperty("fndia");
	private static final String FN_MES = System.getProperty("fnmes");
	private static final String FN_ANO = System.getProperty("fnano");
	
	public  NominasFormPage()
	{
		PageFactory.initElements(SeleniumDriver.getDriver(), this);
		this.url = SeleniumDriver.getBaseUrl() + "gestion/administracion/descarga-nomina/";
		this.url_redirection = SeleniumDriver.getBaseUrl() + "index.php?id=93";
	}
	
	public String getUrl()
	{
		return this.url;
		
	}
	
	public String getUrlRedirection()
	{
		return this.url_redirection;
		
	}
	
	public void insertData()
	{
		this.usuario_input.sendKeys(NominasFormPage.USER);
		this.password_input.sendKeys(NominasFormPage.PWD);
		this.nif_input.sendKeys(NominasFormPage.NIF);
		
		Select selectFnDia = new Select(this.fn_dia_select);
		selectFnDia.selectByVisibleText(NominasFormPage.FN_DIA);
		
		Select selectFnMes = new Select(this.fn_mes_select);
		selectFnMes.selectByVisibleText(NominasFormPage.FN_MES);
		
		Select selectFnAno = new Select(this.fn_ano_select);
		selectFnAno.selectByVisibleText(NominasFormPage.FN_ANO); 
		
		Date date = new Date(System.currentTimeMillis());
		
		String mes = System.getProperty("m");
		if(mes.matches("-?\\d+(\\.\\d+)?")) {
			switch(mes) {
				case "1": 
				case "01":
					mes = "Enero";
					break;
				case "2":
				case "02":
					mes = "Febrero";
					break;
				case "3":
				case "03":
					mes = "Marzo";
					break;
				case "4":
				case "04":
					mes = "Abril";
					break;
				case "5":
				case "05":
					mes = "Mayo";
					break;
				case "6":
				case "06":
					mes = "Junio";
					break;
				case "7":
				case "07":
					mes = "Julio";
					break;
				case "8":
				case "08":
					mes = "Agosto";
					break;
				case "9":
				case "09":
					mes = "Septiembre";
					break;
				case "10":
					mes = "Octubre";
					break;
				case "11":
					mes = "Noviembre";
					break;
				case "12":
					mes = "Diciembre";
					break;
				default: 
					mes = "";
					break;
			}
		}
		else if(mes.equals("") || mes.equals(null)){
			mes = new SimpleDateFormat("MMMM").format(date);
			mes = mes.substring(0,1).toUpperCase() + mes.substring(1).toLowerCase();
		}
		else {
			mes = mes.substring(0,1).toUpperCase() + mes.substring(1).toLowerCase();
		}
		
		Select selectMes = new Select(this.mes_select);
		selectMes.selectByVisibleText(mes);
		
		
		String ano = System.getProperty("y");
		if (ano.equals("") || ano.equals(null)) {
			ano = new SimpleDateFormat("yyyy").format(date);
		}
		Select selectAno = new Select(this.ano_select);
		selectAno.selectByVisibleText(ano);
	}
	
	public void buscar_nomina() {
		this.buscar_button.click();
		
		WebDriverWait wait = new WebDriverWait(SeleniumDriver.getDriver(), 5);
		wait.until(ExpectedConditions.alertIsPresent());

		Alert alert = SeleniumDriver.getDriver().switchTo().alert();
		alert.accept();
	}
	
}
