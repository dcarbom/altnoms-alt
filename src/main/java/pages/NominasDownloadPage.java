package pages;

import java.io.File;
import java.io.FilenameFilter;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.SeleniumDriver;

public class NominasDownloadPage {
	
	private String url;
	private final String DOWNLOAD_PATH = System.getProperty("user.home") + "/Downloads";
	private File dir = new File(this.DOWNLOAD_PATH);
	
	@FindBy(css="#usuario")
	private WebElement usuario_input;
	
	@FindBy(css="#password")
	private WebElement password_input;
	
	@FindBy(css="#nif")
	private WebElement nif_input;
	
	@FindBy(css="#fn_dia")
	private WebElement fn_dia_select;
	
	@FindBy(css="#fn_mes")
	private WebElement fn_mes_select;
	
	@FindBy(css="#fn_ano")
	private WebElement fn_ano_select;
	
	@FindBy(css="#mes")
	private WebElement mes_select;
	
	@FindBy(css="#ano")
	private WebElement ano_select;
	
	@FindBy(css="#c246 > div > div > div > form > table.tabla > tbody > tr > td:nth-child(3) > table > tbody > tr:nth-child(3) > td > a")
	private WebElement download_button;
	
	public  NominasDownloadPage()
	{
		PageFactory.initElements(SeleniumDriver.getDriver(), this);
		this.url = SeleniumDriver.getBaseUrl() + "index.php?id=93";
	}
	
	public String getUrl()
	{
		return this.url;
		
	}
	
	public void download() {
		this.download_button.click();
	}
	
	public int getFilesOnDowloadPath() {
		File[] files = this.dir.listFiles(new FilenameFilter() {
											public boolean accept(File directory, String fileName) {
												return fileName.endsWith(".pdf");
											}
						});
		return files.length;
	}
	
}
