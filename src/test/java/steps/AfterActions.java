package steps;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.cucumber.listener.Reporter;
import com.google.common.io.Files;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import utils.SeleniumDriver;

public class AfterActions {

	@After(order = 1)
	public void afterScenario(Scenario scenario) {
		if (scenario.isFailed()) {
			String screenshotName = scenario.getName().replaceAll(" ", "_");
			WebDriver driver = SeleniumDriver.getDriver();
			try {
				// This takes a screenshot from the driver at save it to the specified location
				File sourcePath = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

				// Building up the destination path for the screenshot to save
				// Also make sure to create a folder 'screenshots' with in the cucumber-report
				// folder
				
				String path = System.getProperty("user.dir") + "/target/Extent_Reports/screenshots/";
				File directory = new File(path);
				File destinationFile = new File(path + screenshotName + ".png");
				
			    if (!directory.exists()){
			        directory.mkdirs();
			    }

				// Copy taken screenshot from source location to destination location
				Files.copy(sourcePath, destinationFile);

				// This attach the specified screenshot to the test
				Reporter.addScreenCaptureFromPath(destinationFile.toString());
			} catch (IOException e) {
			}
		}
	}

	@After(order = 0)
	public static void tearDown(Scenario scenario) {
		SeleniumDriver.tearDown();
	}
}
