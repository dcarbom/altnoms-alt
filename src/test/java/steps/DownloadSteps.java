package steps;

import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.NominasDownloadPage;
import pages.NominasFormPage;
import pages.HomePage;
import pages.LoginPage;
import utils.SeleniumDriver;

public class DownloadSteps {

	private LoginPage loginPage = new LoginPage();
	private HomePage homePage = new HomePage();
	private NominasFormPage nominasFormPage = new NominasFormPage();
	private NominasDownloadPage nominasDownloadPage = new NominasDownloadPage();
	
	private int numberOfFilesOnDownloadFolder;
	private static final String username = System.getProperty("usr");
	private static final String password = System.getProperty("pwd");
	
	@Given("^I am on the login page$")
	public void i_am_on_the_login_page() {
	    SeleniumDriver.openPage(this.loginPage.getUrl());
	    SeleniumDriver.waitForPageToLoad(3000);
	    String actualPage = SeleniumDriver.getDriver().getCurrentUrl();
		String expectedPage = this.loginPage.getUrl();
	    Assert.assertEquals(actualPage, expectedPage, "Error al entrar al login de intranet");
	}
	
	@When("^get into home page$")
	public void get_into_home_page() {
		SeleniumDriver.waitForPageToLoad(2000);
		this.loginPage.usernameInput(DownloadSteps.username);
		this.loginPage.passwordInput(DownloadSteps.password);
		this.loginPage.login();
		SeleniumDriver.waitForPageToLoad(3000);
		String actualPage = SeleniumDriver.getDriver().getCurrentUrl();
		String expectedPage = this.homePage.getUrl();
	    Assert.assertEquals(actualPage, expectedPage, "Error en el login");
	}
	
	@And("^go to nominas page$")
	public void go_to_nominas_page() {
		this.homePage.goToPage(nominasFormPage.getUrl());
		String actualPage = SeleniumDriver.getDriver().getCurrentUrl();
		String expectedPage = this.nominasFormPage.getUrlRedirection();
	    Assert.assertEquals(actualPage, expectedPage, "Error al entrar a la pagina nominas");
	}
	
	@And("^insert all data$")
	public void insert_all_data() {
		this.nominasFormPage.insertData();
		this.nominasFormPage.buscar_nomina();
		SeleniumDriver.waitForPageToLoad(5000);
		String actualPage = SeleniumDriver.getDriver().getCurrentUrl();
		String expectedPage = this.nominasDownloadPage.getUrl();
	    Assert.assertEquals(actualPage, expectedPage, "Error al entrar a la pagina de descarga de nominas");
	}
	
	@And("^click on download$")
	public void click_on_download() {
		this.numberOfFilesOnDownloadFolder = this.nominasDownloadPage.getFilesOnDowloadPath();
		this.nominasDownloadPage.download();
	}
	
	@Then("^pdf is downloaded$")
	public void pdf_is_downloaded() {
		SeleniumDriver.waitForPageToLoad(5000);
		Assert.assertEquals(this.nominasDownloadPage.getFilesOnDowloadPath(), this.numberOfFilesOnDownloadFolder+1, "No se ha descargado ningun archivo");
	}
}
