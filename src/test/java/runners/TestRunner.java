package runners;

import org.testng.annotations.AfterClass;

import com.cucumber.listener.Reporter;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
        plugin = {
        		"json:target/surefire-reports/cucumber-report.json",
        		"usage:target/cucumber-usage.json", 
        		"junit:target/surefire-reports/cucumber-report.xml",
        		"pretty", 
        		"html:target/positive/cucumber.html",
        		"com.cucumber.listener.ExtentCucumberFormatter:target/Extent_Reports/report.html"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = {}       
        )

public class TestRunner extends AbstractTestNGCucumberTests  {
	
	@AfterClass
    public static void setup() {
        Reporter.loadXMLConfig("src/test/resources/extent-config.xml");
    }
	
}
